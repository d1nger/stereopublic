#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdlib.h> 
#include "iostream"
#include <stdio.h>
#include <time.h>
#include <windows.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define W 1980
#define H 1020
#define TX 16
#define TY 16
#define DMIN  -30
#define DMAX 30
#define TILE_DIM 16
#define RADIUS 9

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))
using namespace std;

int _alpha = 0.9, _rgf = 9, _t1 = 7, _t2 = 2, _dlr = 0, _rmwf = 9, _sigma = 9;
float _epsilon = 255 * 255 * 0.0001;
float _sigmac = 255 * 0.0001;
//

//Measure time
double PCFreq;
__int64 CounterStart;

void StartCounter();
double getCounter();

//
int getMaxThreads(){
	/*
	CUDA DEVICE prop OUT
	###################################
	*/
	int deviceCount, device;
	int gpuDeviceCount = 0;
	int _maxCudaThreads, _maxCudaProcs, _maxCudaShared, _maxSharedPerBlock;
	struct cudaDeviceProp properties;
	cudaError_t cudaResultCode = cudaGetDeviceCount(&deviceCount);
	if (cudaResultCode != cudaSuccess)
		deviceCount = 0;
	/* machines with no GPUs can still report one emulation device */
	for (device = 0; device < deviceCount; ++device) {
		cudaGetDeviceProperties(&properties, device);
		if (properties.major != 9999) /* 9999 means emulation only */
			if (device == 0)
			{
				_maxCudaProcs = properties.multiProcessorCount;
				_maxCudaThreads = properties.maxThreadsPerBlock;
				_maxCudaShared = properties.sharedMemPerMultiprocessor;
				_maxSharedPerBlock = properties.sharedMemPerBlock;
				printf("\n GPU Stats \n # Cuda Processors: %d\n", _maxCudaProcs);
				printf("#max Threads Per Block: %d \n", _maxCudaThreads);
				printf("#Max of shared per block %d\n", _maxSharedPerBlock);
				
			}
	}
	// ----###########################
	return _maxCudaThreads;
};
int getMaxShared(){
	/*
	CUDA DEVICE prop OUT
	###################################
	*/
	int deviceCount, device;
	int gpuDeviceCount = 0;
	int  _maxCudaShared;
	struct cudaDeviceProp properties;
	cudaError_t cudaResultCode = cudaGetDeviceCount(&deviceCount);
	if (cudaResultCode != cudaSuccess)
		deviceCount = 0;
	/* machines with no GPUs can still report one emulation device */
	for (device = 0; device < deviceCount; ++device) {
		cudaGetDeviceProperties(&properties, device);
		if (properties.major != 9999) /* 9999 means emulation only */
			if (device == 0)
			{
				_maxCudaShared = properties.sharedMemPerBlock;

			}
	}
	// ----###########################
	return _maxCudaShared;
};


int* intergralImage(unsigned char* in, int x, int y) {
	int* result = new int[x*y];
	for (int j = 0; j<x; j++) {
		for (int i = 0; i <y; i++) {

			if (i != 0 && j != 0) {
				result[i + j*y] = result[(i - 1) + j*y] + result[i + (j - 1)*y] - result[(i - 1) + (j - 1)*y] + in[i + j*y];
			}
			else {
				if (i == 0 && j == 0) {
					result[i + j*y] = in[i + j*y];
				}
				else if (i == 0) {
					result[i + j*y] = in[i + j*y] + result[i + (j - 1)*y];
				}
				else if (j == 0) {
					result[i + j*y] = in[i + j*y] + result[(i - 1) + j*y];
				}
				else {
					printf("Problem ij indexing \n");
				}
			}

		}
	}
	return result;
};
int* intergralInt(int* in, int x, int y) {
	int* result = new int[x*y];
	for (int j = 0; j<x; j++) {
		for (int i = 0; i <y; i++) {

			if (i != 0 && j != 0) {
				result[i + j*y] = result[(i - 1) + j*y] + result[i + (j - 1)*y] - result[(i - 1) + (j - 1)*y] + in[i + j*y];
			}
			else {
				if (i == 0 && j == 0) {
					result[i + j*y] = in[i + j*y];
				}
				else if (i == 0) {
					result[i + j*y] = in[i + j*y] + result[i + (j - 1)*y];
				}
				else if (j == 0) {
					result[i + j*y] = in[i + j*y] + result[(i - 1) + j*y];
				}
				else {
					printf("Problem ij indexing \n");
				}
			}

		}
	}
	return result;
};

int* intergralFloat(float* in, int x, int y) {
	int* result = new int[x*y];
	for (int j = 0; j<x; j++) {
		for (int i = 0; i <y; i++) {

			if (i != 0 && j != 0) {
				result[i + j*y] = result[(i - 1) + j*y] + result[i + (j - 1)*y] - result[(i - 1) + (j - 1)*y] + in[i + j*y];
			}
			else {
				if (i == 0 && j == 0) {
					result[i + j*y] = in[i + j*y];
				}
				else if (i == 0) {
					result[i + j*y] = in[i + j*y] + result[i + (j - 1)*y];
				}
				else if (j == 0) {
					result[i + j*y] = in[i + j*y] + result[(i - 1) + j*y];
				}
				else {
					printf("Problem ij indexing \n");
				}
			}

		}
	}
	return result;
};


__global__ void colorDeltaGPU(unsigned char* left, unsigned char* right, unsigned char* delta){
	int _t1 = 7;
	int idx = (blockIdx.y*gridDim.x + blockIdx.x)*(blockDim.x*blockDim.y) + threadIdx.y*blockDim.x + threadIdx.x;
	unsigned char temp = (abs(left[idx * 3 + 0] - right[idx * 3 + 0]) + abs(left[idx * 3 + 1] - right[idx * 3 + 1]) + abs(left[idx * 3 + 2] - right[idx * 3 + 2])) / 3;
	temp = temp < _t1 ? _t1 : temp;
	temp > 255 ? delta[idx] = 255 : delta[idx] = temp;
}

__global__ void gradGPU(unsigned char* left, unsigned char* right, unsigned char* grad, int height, int width){
	int _t2 = 2;
	int idx = (blockIdx.y*gridDim.x + blockIdx.x)*(blockDim.x*blockDim.y) + threadIdx.y*blockDim.x + threadIdx.x;
	int grad_n;
	float grad_r, grad_l;
	if (idx == 0 || idx%(width+1)==0){
		grad_l = (left[idx + 1] - left[idx]) / 2;
		grad_r = (right[idx + 1] - left[idx]) / 2;
	}
	else if (idx == width*height || idx % width == 0){
		grad_l = (left[idx] - left[idx - 1]) / 2;
		grad_r = (right[idx] - left[idx - 1]) / 2;
	}
	else{
		grad_l = (left[idx + 1] - left[idx - 1]) / 2;
		grad_r = (right[idx + 1] - left[idx - 1]) / 2;
	}

	grad_n = (int)abs(grad_l - grad_r);
	grad[idx] = (grad_n < _t2) ? grad_n : _t2;
	
}

__global__ void integralGPU_char( unsigned char* in, int* out, int w, int h) {
	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y;
	float temp = 0;
	if (x < w && y < h) {
		// The first loop iterates from zero to the Y index of the thread which represents the corresponding element of the output/input array.  
		for (int r = 0; r <= y; r++)
		{
			// The second loop iterates from zero to the X index of the thread which represents the corresponding element of the output/input array  
			for (int c = 0; c <= x; c++)
			{
				temp = temp + in[r*w + c];
			}
		}
	}

	out[x + w*y] = temp;
}


__global__ void squareI2(unsigned char* in, int* out) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	if (idx == 0) {
		printf("Getting the square GPU\n");
	}
	out[idx] = in[idx]* in[idx];
}

__global__ void meanKernelFloat(float* out, float* in, int w, int h) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	if (idx == 0) {
		printf("Calcualting the mean GPU \n");
	}
	if ((idx + RADIUS) % w < 2 * RADIUS || (idx - w*(RADIUS + 1))<0 || (idx + RADIUS*w > w*h)) {
		out[idx] = 50;
	}
	else {
		int sum = in[idx + RADIUS*w + RADIUS]
			- in[idx - 1 + RADIUS*w - RADIUS]
			- in[idx + RADIUS - w*(RADIUS + 1)]
			+ in[idx - RADIUS - 1 - (RADIUS + 1)*(w)];
		out[idx] = sum / ((RADIUS * 2 + 1)*(RADIUS * 2 + 1));
	}
}

__global__ void meanKernel(unsigned char* out, int* in, int w, int h) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	if (idx == 0) {
		printf("Calcualting the mean GPU \n");
	}
	if ((idx + RADIUS) % w < 2 * RADIUS || (idx - w*(RADIUS+1))<0 || (idx + RADIUS*w > w*h)){
		out[idx] = 50;
	}
	else {
		int sum = in[idx + RADIUS*w + RADIUS]
			- in[idx - 1 + RADIUS*w -RADIUS ]
			- in[idx + RADIUS - w*(RADIUS+1)]
			+ in[idx - RADIUS - 1 - (RADIUS+1)*(w)];
		out[idx] = sum /((RADIUS*2+1)*(RADIUS * 2 + 1));
	}
}

__global__ void variKernel(unsigned char* out, int* in, unsigned char* mean, int w, int h) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	if (idx == 0) {
		printf("Calcualting the vARIANCE GPU\n");
	}
	if ((idx + RADIUS) % w < 2 * RADIUS || (idx - w*(RADIUS + 1))<0 || (idx + RADIUS*w > w*h)) {
		out[idx] = 220;
	}
	else {
		int sum = in[idx + RADIUS*w + RADIUS]
			- in[idx - 1 + RADIUS*w - RADIUS]
			- in[idx + RADIUS - w*(RADIUS + 1)]
			+ in[idx - RADIUS - 1 - (RADIUS + 1)*(w)];
		out[idx] = (sum + mean[idx]*mean[idx]) / ((RADIUS * 2 + 1)*(RADIUS * 2 + 1));
	}
}

__global__ void infCost(float* out) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	out[idx] = (float)255;
}

__global__ void kernelGrad(unsigned char* in_left, unsigned char* in_right, float* out, int w, int h, int d) {
	int t2 = 2;
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	
	if ( (idx + 1 + abs(d)) % w < (DMAX + 1) || (idx +d -1)<0 ){
		out[idx] = t2;
	}
	else {
		float l_grad = abs(in_left[idx-1] - in_left[idx+1]) / 2;
		float r_grad = abs(in_right[idx + d -1] - in_right[idx + d + 1]) / 2;

		float grad_n = abs(l_grad - r_grad);
		grad_n > t2 ? out[idx] = t2 : out[idx] = grad_n;
	}

}

__global__ void kernelColor(unsigned char* in_left, unsigned char* in_right, float* out, int w, int h, int d) {
	int t1 = 7;

	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	if (idx == 0) {
		printf("Calcualting the Color GPU %d\n", d);
	}
	if ((idx + abs(d))*3 % w < ((DMAX)*3 )) {
		out[idx] = t1;
	}
	else {
		int temp = (abs(in_left[idx*3+0] - in_right[(idx+d)* 3 + 0 ])
			+ abs(in_left[idx * 3 + 1] - in_right[(idx+d) * 3 + 1])
			+ abs(in_left[idx * 3 + 2] - in_right[(idx+d) * 3 + 2])) / 3;

		temp > t1 ? out[idx] = t1 : out[idx] = temp;
	}

}

__global__ void kernelCost(float* color, float *grad, int w, int h, float* out, int d) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	float alpha = 0.9;
	if (idx < w*(h-1) ) {
		out[idx] = (1 - alpha)* color[idx] + alpha * grad[idx];;
	}// 
}

__global__ void kernelCompareCost(float* new_cost, float* cost, unsigned char* disp, int d, int w, int h) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	
	if (idx <(w-1)*(h - 1)) {
		if (cost[idx] > new_cost[idx]) {
			cost[idx] = new_cost[idx];
			disp[idx] = d+70;
		}
	}

}
__global__ void testKernel() {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	if (idx == 0) {
		printf("Calcualting the TEST GPU %d\n", idx);
	}
};

__global__ void kernelBoxFilter(float * in , float * out, int w, int h) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	const int shared_size = (2*RADIUS+TILE_DIM)*(2*RADIUS + TILE_DIM);
	__shared__ float s_right[shared_size];
	
	int tile_indx = blockIdx.x*TILE_DIM*TILE_DIM;
	// Tile start ->(blockIdx.y*gridDim.x + blockIdx.x)*(TILE_DIM*TILE_DIM)
	//From main to shared
	
	if (idx == 0) {
		for (int i = 0; i < (2 * RADIUS + TILE_DIM); i++) {
			for (int j = 0; j < (2*RADIUS + TILE_DIM);j++) {
				int idx_in = tile_indx + i - RADIUS + (j - RADIUS)*w;
				if (idx_in >= 0 && idx_in%w < RADIUS && idx_in<w*h) {
					s_right[i + j*(2 * RADIUS + TILE_DIM)] =  in[idx_in];
				}
				else {
					s_right[i + j*(2 * RADIUS + TILE_DIM)] = 0;
				}
			}
		}
	}
	__syncthreads();
	out[idx] = 1;
	// Disparity calculations&	

	/*
	int idx_out = threadIdx.y * blockDim.x + threadIdx.x;
	int sum = 0;
	for (int i = 0; i < 2* RADIUS; i++) {
		for (int j = 0; i <  2*RADIUS; j++) {
			//sum += s_right[idx_out%(2 * RADIUS + TILE_DIM)];
		}
	}

	out[idx] = 2;
	*/
	
};
__global__ void multKernel(float* cost, unsigned char* left, float* out, int w, int h) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	out[idx] = cost[idx] * left[idx];
};

__global__ void kernelA(float * mMean, float* pMean, unsigned char* mean, unsigned char* var , int w , int h, float* a) {
	float _epsilon = 255 * 255 * 0.0001;
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	a[idx] = (mMean[idx] - pMean[idx] * mean[idx]) / (var[idx]+ _epsilon);
};

__global__ void kernelB(float * a, float* pMean, unsigned char* mean,  float* b) {
	float _epsilon = 255 * 255 * 0.0001;
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	b[idx] = pMean[idx] - mean[idx]*a[idx];
};
__global__ void kernelQ(float * a, float* b, unsigned char* img, float* q) {
	int idx = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
	q[idx] = a[idx]*img[idx] + b[idx];
};
cudaError_t gpuDisparity(unsigned char* color_left, unsigned char* color_right, unsigned char* bw_left, unsigned char* bw_right, int width, int height, int size,int* integralLeft, int* integralSqaure, unsigned char* disparityCPU) {
	cudaError_t cudaStatus=cudaSuccess;
	// Threads Data: Th/Block, BlockSize, GridSize
	int max_threads = getMaxThreads();
	//printf("BX %d and BY %d and ", bx, by);
	const dim3 dim_block(TX, TY);
	const int bx = (W + TX - 1) / TX;
	const int by = (W + TY - 1) / TY;
	//const dim3 grid_size = dim3(bx, by); mARCHE PAS ggrid 2D?
	const int grid_size = bx*by; 

	//CUDA Memory Allocation for initial input : 2bw and 2 color images, and the output
	unsigned char *dev_left_color, *dev_right_color, *dev_left_bw, *dev_right_bw, *disparity;
	cudaMalloc((void**) &dev_left_bw, width*height);
	cudaMalloc((void**) &dev_right_bw, width*height);
	
	cudaMalloc((void**) &disparity, width*height);
	cudaMalloc((void**) &dev_left_color, size);
	cudaMalloc((void**) &dev_right_color, size);

	//Copying from host to device of the inptu
	cudaMemcpy(dev_right_color, color_left, size , cudaMemcpyHostToDevice);
	cudaMemcpy(dev_left_color, color_right, size , cudaMemcpyHostToDevice);
	cudaMemcpy(dev_right_bw, bw_right, width*height , cudaMemcpyHostToDevice);
	cudaMemcpy(dev_left_bw, bw_left, width*height, cudaMemcpyHostToDevice);
	
	int *intI;
	if (cudaMalloc((void**)&intI, width*height * sizeof(int)) != cudaSuccess) {
		printf("Bad allocation intI \n");
	}; 
	int *intI2;
	if (cudaMalloc((void**)&intI2, width*height * sizeof(int)) != cudaSuccess) {
		printf("Bad allocation intI2 \n ");
	};
	
	//Call Kernel for integral  I	OK
	//Call Kernel for I^2			OK
	if (cudaMemcpy(intI, integralLeft, width*height * sizeof(int), cudaMemcpyHostToDevice) != cudaSuccess) { printf("BAD COPY"); }
	if (cudaMemcpy(intI2, integralSqaure, width*height * sizeof(int), cudaMemcpyHostToDevice) != cudaSuccess) { printf("BAD COPY"); };
	//Call Kernel for integral I^2
	//STEP1 : Calcualting Mean and Var
	unsigned char *meanI, *varI;
	float* costCPU = new float[width*height];
	for (int i = 0;i < width*height; i++) {
		costCPU[i] = 200;
	}
	if (cudaMalloc((void**)&meanI, width*height * sizeof(unsigned char)) != cudaSuccess) {
		printf("Memory alocation error");
	};
	
	if (cudaMalloc((void**)&varI, width*height * sizeof(unsigned char)) != cudaSuccess) {
		printf("Memory alocation error");
	};

	float* cost;
	if (cudaMalloc((void**)&cost, width*height *sizeof(float)) != cudaSuccess) {
		printf("Memory alocation error");
	};
	if (cudaMemcpy(cost, costCPU, width*height * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) { printf("BAD COPY COST"); };

	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "KERNEL Mean failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}
	//Call Kernel for mean I			OK
	//Call Kernel for variance I		OK
	//Call kernel on cost				OK
	
	meanKernel <<< grid_size, dim_block >>>(meanI, intI, width, height);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "KERNEL Mean failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}	
	variKernel <<< grid_size, dim_block >>>(varI, intI2, meanI, width, height);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "KERNEL Mean failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}	
	

	//STEP3: Iterate through DIMS
	for (int d = DMIN; d < DMAX; d++) {
		//Calculating Delta Color and Grad

		//Call Kernel CoLOR		OK
		//Call Kernel GRAD		OK 
		//Call Kernel Cost		OK
		//Call kernel compare Cost OK
		float *deltaCol, *deltaGrad;
		cudaMalloc((void**)&deltaCol, width*height * sizeof(float));
		cudaMalloc((void**)&deltaGrad, width*height * sizeof(float));
		kernelGrad << < grid_size, dim_block >> >(dev_left_bw, dev_right_bw, deltaGrad, width, height, d);
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "KERNEL Grad failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}
		
		kernelColor << < grid_size, dim_block >> >(dev_left_color, dev_right_color, deltaCol, width, height, d);
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "KERNEL Color failed on color: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}

		//BoxFilter Slice P
		float *slicePi;
		cudaMalloc((void**)&slicePi, width*height * sizeof(float));
		kernelCost << < grid_size, dim_block >> >(deltaCol, deltaGrad, width, height, slicePi, d);
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "KERNEL Cost failed on cost: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}

		// Filtering of cost
		float* pFiltered;
		cudaMalloc((void**)&pFiltered, width*height * sizeof(float));
		kernelBoxFilter << < grid_size, dim_block >> >(slicePi, pFiltered, width, height);

		float* mSliceImg;
		if (cudaMalloc((void**)&mSliceImg, width*height * sizeof(float)) != cudaSuccess) { printf("Not Success"); };
		multKernel << < grid_size, dim_block >> >(slicePi, dev_left_bw, mSliceImg, width, height);

		float* mFiltered;
		if (cudaMalloc((void**)&mFiltered, width*height * sizeof(float)) != cudaSuccess) { printf("Not Success"); };
		kernelBoxFilter << < grid_size, dim_block >> >(mSliceImg, mFiltered, width, height);

		float *a, *b, *q, *aFiltered, *bFitlered;
		if (cudaMalloc((void**)&a, width*height * sizeof(float)) != cudaSuccess) { printf("Not Success"); };
		if (cudaMalloc((void**)&b, width*height * sizeof(float)) != cudaSuccess) { printf("Not Success"); };
		if (cudaMalloc((void**)&q, width*height * sizeof(float)) != cudaSuccess) { printf("Not Success"); };
		if (cudaMalloc((void**)&aFiltered, width*height * sizeof(float)) != cudaSuccess) { printf("Not Success"); };
		if (cudaMalloc((void**)&bFitlered, width*height * sizeof(float)) != cudaSuccess) { printf("Not Success"); };

		kernelA << < grid_size, dim_block >> >(mFiltered, pFiltered, meanI, varI, width, height, a);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "KERNEL Cost2 failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}	
		kernelB << < grid_size, dim_block >> >(a, pFiltered, meanI, b);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "KERNEL Cost2 failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}
		kernelQ << < grid_size, dim_block >> >(a, b, dev_left_bw, q);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "KERNEL Cost2 failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}

		kernelCompareCost << < grid_size, dim_block >> >(slicePi, cost, disparity, d, width, height);
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "KERNEL Cost2 failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}
		//
		cudaFree(deltaCol);
		cudaFree(deltaGrad);
		cudaFree(a);
		cudaFree(b); 
		cudaFree(q);
		cudaFree(aFiltered);
		cudaFree(bFitlered);
		cudaFree(mFiltered);
		cudaFree(mSliceImg);
		cudaFree(pFiltered);
	}
	cudaMemcpy(disparityCPU, disparity, width*height, cudaMemcpyDeviceToHost);


	cudaFree(varI);
	cudaFree(meanI);
	cudaFree(cost);
	cudaFree(intI);
	cudaFree(dev_right_color);
	cudaFree(dev_left_color);
	cudaFree(disparity);
	cudaFree(dev_right_bw);
	cudaFree(dev_left_bw);

Error:
	return cudaStatus; 
}

void ToBlack(unsigned char* in, unsigned char* out, int x, int y) {
	unsigned char  temp;
	//Conversion from Color to Black and White
	for (int i = 0; i < x*y; i++) {
		temp = 0.299*in[i * 3 + 0] + 0.587*in[i * 3 + 1] + 0.0721*in[i * 3 + 2];
		temp > 255 ? out[i] = 255 : out[i] = temp;
	}
	printf("Calculated Black and White! \n");
};


void StartCounter(){
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
		std::cout << "QueryPerformanceFrequency failed! \n";

	PCFreq = double(li.QuadPart);// / 1000000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}

double getCounter(){
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart - CounterStart) / PCFreq;
}


// Main function
int main() {
	char* _filename = "in_left.png";
	char* _filename_2 = "in_right.png";
	char* _out1 = "delta_color.bmp";
	char* _out3 = "cost_disparity_cpu.bmp";
	char* _out4 = "cost_disparity_gpu.bmp";
	//
	int x, y, z;
	unsigned char *img_left = stbi_load(_filename, &x, &y, &z, 3);
	unsigned char *img_right = stbi_load(_filename_2, &x, &y, &z, 3);
	//int stbi_write_bmp(char const *filename, int w, int h, int comp, const void *data);
	//unsigned char img_left_b[SIZEX*SIZEY*CHANNEL];
	int size = x*y*z;
	unsigned char *img_left_b = new unsigned char[x*y];
	unsigned char *img_right_b = new unsigned char[x*y];
	unsigned char *img_delta = new unsigned char[x*y];
	unsigned char* img_gradient = new unsigned char[x*y];
	unsigned char* disparity = new unsigned char[x*y];
	unsigned char *img_cost = new unsigned char[x*y];
	unsigned char* disparityCPU = new unsigned char[x*y];

	//Convert to black and white
	ToBlack(img_left, img_left_b, x, y);
	ToBlack(img_right, img_right_b, x, y);

	//Reset counter
	PCFreq = 0.0;
	CounterStart = 0;
	clock_t start_t = clock();
	StartCounter();
	//Cpu Algorithm
	printf("Time taken: %f \n", getCounter());

	//GPU prep, call GPU fct
	cudaError_t cudaStatus;
	unsigned char* d_disparity = new unsigned char[x*y];
	cudaDeviceSynchronize();
	int* integralLeft = intergralImage(img_left_b, x, y);
	int* doubleImg = new int[x*y];
	for (int j = 0; j<x; j++) {
		for (int i = 0; i<y; i++) {
			doubleImg[i + j*y] = img_left_b[i + j*y] * img_left_b[i + j*y];
		}
	}
	int* integralSquare = intergralInt(doubleImg, x, y);
	cudaStatus = gpuDisparity(img_left, img_right, img_left_b, img_right_b, x, y, size, integralLeft, integralSquare, disparityCPU);	
	int write2 = stbi_write_bmp("Cost Map.bmp", x, y, 1, disparityCPU);

	stbi_image_free(img_left);
	stbi_image_free(img_right);
	stbi_image_free(d_disparity);

	return 0;
};

