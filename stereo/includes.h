#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdlib.h> 
#include "iostream"
#include <stdio.h>
#include <time.h>
#include <windows.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define WIDTH 1980
#define HEIGHT 1020
#define DMIN  -30
#define DMAX 30
#define BLOCK_SIZE 32
#define GRID_SIZE 4
#define TILE_DIM 32
#define BLOCK_ROW 8
#define RADIUS 9
#define DISPARITY_RANGE 40

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))