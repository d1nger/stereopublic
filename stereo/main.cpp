#include <stdlib.h> 
#include "iostream"
#include <stdio.h>
#include <time.h>
#include <windows.h>
#include <sstream>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


#define DMIN  -30
#define DMAX 30
#define BLOCK_SIZE 32
#define GRID_SIZE 4
#define TILE_DIM 32
#define BLOCK_ROW 8
#define RADIUS 9

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))
using namespace std;

int _alpha = 0.9, _rgf = 9, _t1 = 7, _t2 = 2, _dlr = 0, _rmwf = 9, _sigma = 9;
float _epsilon = 255 * 255 * 0.0001;
float _sigmac = 255 * 0.0001;
void ToBlack(unsigned char* in, unsigned char* out, int x, int y);
unsigned char* DeltaColor(unsigned char* left, unsigned char* right, unsigned char* out, int x, int y,  int d);
unsigned char* GetGradient(unsigned char *img_left, unsigned char *img_right, int x, int y,  int d);
unsigned char* getVar(unsigned int *in, int x, int y,  unsigned char* mean);
unsigned char* getMean(unsigned int *in, int x, int y);
int* intergralInt(int* in, int x, int y, int size);

void DisparityNew( unsigned char* left, unsigned char* right, unsigned char* color_l, unsigned char* color_r, unsigned char* output, int x, int y);


// Main function
int main2(){
	const char* _filename = "img2.png";
	const char* _filename_2 = "img1.png";
	const char* _out1 = "delta_color.bmp";
	const char* _out3 = "cost_disparity_cpu.bmp";
	const char* _out4 = "cost_disparity_gpu.bmp";
	//
	int x, y, z;
	unsigned char *img_left = stbi_load(_filename, &x, &y, &z, 3);
	unsigned char *img_right = stbi_load(_filename_2, &x, &y, &z, 3);
	int size = x*y*z;
	unsigned char *img_left_b = new unsigned char[x*y];
	unsigned char *img_right_b = new unsigned char[x*y];

	//Convert to black and white
	ToBlack(img_left, img_left_b,x,y);
	ToBlack(img_right, img_right_b,x,y);
	//Reset counter

	unsigned char* new_disp = new unsigned char[x*y];
	DisparityNew( img_left_b, img_right_b, img_left, img_right, new_disp, x, y);

	//CPU Free
	stbi_image_free(img_left);
	stbi_image_free(img_right);
	free(img_left_b);
	free(img_right_b);
	//free(d_disparity);
	
	return 0;
};
void ToBlack(unsigned char* in, unsigned char* out, int x, int y){
	unsigned char  temp;	
	//Conversion from Color to Black and White
	for (int i = 0; i < x*y; i++){
		temp = 0.299*in[i * 3 + 0] + 0.587*in[i * 3 + 1] + 0.0721*in[i * 3 + 2];
		temp > 255 ? out[i] = 255 : out[i] = temp;
	}
	printf("Calculated Black and White! \n");
}

unsigned char* DeltaColor(unsigned char* left, unsigned char* right, int x, int y,  int d){
	unsigned char temp;
	unsigned char* out = new unsigned char[x*y];
	//for (int j=0; j< x; j++){
		int j=0;
		for(int i=0; i<x*y; i++){
			int idx = MIN((i+d),y);
			idx = MAX(idx, 0);
			temp =(abs(left[i*3+0+j*y] - right[(idx)*3+0+j*y]) + abs(left[i*3+1+j*y] - right[idx*3+1+j*y]) + abs(left[i*3+2+j*y] - right[idx*3+2+j*y]))/3;
			temp = temp < _t1 ? _t1 : temp;
			temp > 255 ? out[i] = 255 : out[i] = temp;
		}
	//}
	return out;
}

unsigned char* GetGradient(unsigned char *img_left, unsigned char *img_right, int x, int y,  int d){

	float l_grad, r_grad;
	unsigned char* out = new unsigned char[x*y];
	for(int j=0; j<x;j++){
		for (int i = 0; i < y; i++){
			int idxl_max = MIN(i+1,y);
			int idxl_min = MAX(i-1,0);
			int idxr_min = MAX(i+d-1, 0);
				idxr_min = MIN(idxr_min, y);
			int idxr_max = MIN(i+d+1, y);
				idxr_max = MAX(i + d + 1, 0);

			l_grad = abs(img_left[idxl_max+j*y] - img_left[idxl_min+j*y])/2;
			r_grad = abs(img_right[idxr_max+j*y] - img_right[idxr_min+j*y])/2;

			float grad_n = abs(l_grad - r_grad);
			out[i+j*y] = (char)MAX(grad_n, _t2);
		}
	}
	return out;
}


int* intergralImage(unsigned char* in, int x, int y){
	 int* result= new int[x*y];
	for(int j=0; j<x; j++){
		for(int i=0; i <y ; i++){

			if(i!=0 && j!=0){
				result[i+j*y] = result[(i-1)+j*y] + result[i+(j-1)*y] -result[(i-1)+(j-1)*y] + in[i+j*y];
			}else{
				if(i==0 && j==0){
					result[i+j*y] = in[i+j*y];
				}else if(i==0){
					result[i+j*y] = in[i+j*y] + result[i+(j-1)*y];
				}else if(j==0){
					result[i+j*y] = in[i+j*y] + result[(i-1)+j*y];
				}else{
					printf("Problem ij indexing \n");
				}
			}

		}
	}
	return result;	
};
int* intergralInt(int* in, int x, int y){
	 int* result= new int[x*y];
	for(int j=0; j<x; j++){
		for(int i=0; i <y ; i++){

			if(i!=0 && j!=0){
				result[i+j*y] = result[(i-1)+j*y] + result[i+(j-1)*y] -result[(i-1)+(j-1)*y] + in[i+j*y];
			}else{
				if(i==0 && j==0){
					result[i+j*y] = in[i+j*y];
				}else if(i==0){
					result[i+j*y] = in[i+j*y] + result[i+(j-1)*y];
				}else if(j==0){
					result[i+j*y] = in[i+j*y] + result[(i-1)+j*y];
				}else{
					printf("Problem ij indexing \n");
				}
			}

		}
	}
	return result;	
};
unsigned char* getMean( int* in, int x, int y){
	unsigned char* mean = new unsigned char[x*y];

	for(int j=0; j<x ; j++){
		for(int i=0; i<y; i++){

			int idx_ix = MAX(i - RADIUS, 0);
			int idx_iy = MAX(j - RADIUS, 0);
			int idx_jx = MIN(i + RADIUS, y-1);
			int idx_jy = MIN(j + RADIUS, x-1);
			int idx_ix_1 = MAX(idx_ix-1, 0);
			int idx_iy_1 = MAX(idx_iy-1, 0);
			int w = (idx_jx - idx_ix)*(idx_jy-idx_iy);
			
			int sum = in[idx_jx + idx_jy*y] 
						- in[idx_ix_1+idx_jy*y] 
						- in[idx_jx + idx_iy_1*y] 
						+ in[idx_ix_1+ idx_iy_1*y];

			mean[i+j*y] = (sum/w);
		}
	}
	return mean;
}
unsigned char* getVar(int* in, int x, int y,  unsigned char* mean){
	//printf("Calcualting Mean And Variacne \n");
	//Padding

	unsigned char* vari = new unsigned char[x*y];
	for(int j=0; j<x ; j++){
		for(int i=0; i<y; i++){

			int idx_ix = MAX(i - RADIUS, 0);
			int idx_iy = MAX(j - RADIUS, 0);
			int idx_jx = MIN(i + RADIUS, y-1);
			int idx_jy = MIN(j + RADIUS, x-1);
			int idx_ix_1 = MAX(idx_ix - 1, 0);
			int idx_iy_1 = MAX(idx_iy - 1, 0);
			int w = (idx_jx - idx_ix)*(idx_jy-idx_iy);
			
			int sum = in[idx_jx + idx_jy*y] 
						- in[idx_ix_1+idx_jy*y] 
						- in[idx_jx + idx_iy_1*y] 
						+ in[idx_ix_1+ idx_iy_1*y];
	
			vari[i+j*y] = (sum - (int)mean[i+j*y] *(int)mean[i+j*y] )/w;
		}
	}


	return vari;
}
void DisparityNew( unsigned char* left, unsigned char* right, unsigned char* color_l, unsigned char* color_r,  unsigned char* output, int x, int y){

	//Image integrale LEFT_IMAGE
	int* integral = intergralImage(left, x,y);
	//Calcule de I² 	=> || 
	int* doubleImg = new int[x*y];
	for(int j=0; j<x ; j++){
		for(int i=0; i<y; i++){
			doubleImg[i+j*y] = left[i+j*y]*left[i+j*y];
		}
	}
	//Image integrale de I² 
	int* integralDouble = intergralInt(doubleImg,x,y);

	//Etape 1: Mean and Var for I 	=> 
	unsigned char* mean = getMean(integral, x, y); 
	unsigned char* var =  getVar(integralDouble, x, y, mean);
	
	//Etape 2: initialisation de coup a maxiumum
	unsigned char* out = new unsigned char[x*y];
	unsigned char* disparity = new unsigned char[x*y];
		for(int i=0; i<x*y; i++){
			disparity[i]=255;
		}

	//Etape 3: Calcule de volume de disparite de [dmin, dmax];
	for(int k= DMIN; k<=DMAX; k++){
		// Compute the cost volume for each D	
		std::string name_s ;
		std::ostringstream oss;
		oss << k;
		name_s += oss.str();
		name_s += "_col_d";
		name_s.append(".bmp");
		char* name = (char*) name_s.c_str();
		//printf("Calculating for %i \n", k);
		unsigned char* colorD = DeltaColor(color_l, color_r, x, y, k);
		unsigned char* gradD = GetGradient(left, right, x, y, k);
		unsigned char* curr_cost = new unsigned char[x*y];
		for(int j =0; j<x; j++){
			for(int i=0; i<y; i++){
				int idx = MIN((i+k),y);
				idx = MAX(idx, 0);
				curr_cost[i+j*y] = (1 -_alpha)*colorD[idx+j*y] + _alpha*gradD[idx+j*y]; 
			}
		}

		//int write4 = stbi_write_bmp(name, x, y, 1, curr_cost);	
		
		// Filtering of cost by guided filter
		int* p_integral = intergralImage(curr_cost, x , y);
		unsigned char* p_mean = getMean(p_integral, x,y);
		
		unsigned char* mult_cost = new unsigned char[x*y];
		for(int j =0; j<x; j++){
			for(int i=0; i<y; i++){
				mult_cost[i+j*y] = curr_cost[i+j*y]*left[i+j*y]; 
			}
		}

		int* m_integral = intergralImage(mult_cost, x ,y);
		unsigned char* m_mean  = getMean(m_integral, x, y);
		unsigned char* a= new unsigned char[x*y];
		unsigned char* b= new unsigned char[x*y];
		for(int j=0; j<x; j++){
			for(int i=0; i<y; i++){
				a[i+j*y]=(unsigned char)(((float)m_mean[i+j*y]-(float)p_mean[i+j*y]*mean[i+j*y])/((float)var[i+j*y]+_epsilon));
				b[i+j*y]=p_mean[i+j*y]-a[i+j*y]*mean[i+j*y];
			}
		}
		int *int_a = intergralImage(a, x, y);
		int* int_b = intergralImage(b, x, y);		
		unsigned char* mean_a = getMean(int_a, x,y);
		unsigned char* mean_b = getMean(int_b,x,y);
		//printf("finished a and bt \n ");
		unsigned char* q = new unsigned char[x*y];
		for (int j=0; j<x; j++){
			for(int i=0; i<y; i++){
				q[i+j*y]=mean_a[i+j*y]*left[i+j*y]+mean_b[i+j*y];
			}
		}
		if( k == -5){

			int write4 = stbi_write_bmp("unFiltered d5.png", x, y, 1, curr_cost);
			int write5 = stbi_write_bmp("Filtered d5.png", x, y, 1, q);
		}
	    //printf("finished Q \n ");
		// Disparity selection
		
		for (int j=0; j<x; j++){
			for(int i=0; i<y; i++){
				if(disparity[i+j*y] > q[i+j*y]){
					disparity[i+j*y] = q[i+j*y];
					out[i+j*y] = k+123;				
				}	
			}
		}
		/*
		for (int j=0; j<x; j++){
			for(int i=0; i<y; i++){
				if(disparity[i+j*y] > curr_cost[i+j*y]){
					disparity[i+j*y] = curr_cost[i+j*y];
					out[i+j*y] = k+100;				
				}	
			}
		}*/
		free(colorD);
		free(gradD);
		free(curr_cost);
		free(p_integral);
		free(p_mean);
		free(a);
		free(b);
		free(int_a);
		free(int_b);
		free(mean_a);
		free(mean_b);
		free(q);
	}

	int write1 = stbi_write_bmp( "Disp1.bmp", x, y, 1,out);
	int write2 = stbi_write_bmp( "Cost2.bmp", x, y, 1,disparity);
};
